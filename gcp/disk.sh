#!/bin/bash

# disk
if [ $(lsblk -ln -o NAME | grep sdb | wc -l) -gt 0 ];then
    # partion
    is_exist=$(lsblk -ln -o NAME | grep sdb1 | wc -l)
    if [ "${is_exist}" -eq 0 ]; then
    sudo fdisk /dev/sdb << EOF
n
p
1


wq
EOF
    fi

    # format
    sdb1_uuid=$(lsblk -fln -o UUID /dev/sdb1)
    if [ "${sdb1_uuid}" == "" ]; then
      sudo mkfs.xfs /dev/sdb1
    fi

    # mount
    sdb1_uuid=$(lsblk -fln -o UUID /dev/sdb1)
    if [ "${sdb1_uuid}" != "" ]; then
       is_exist=$(cat /etc/fstab | grep ${sdb1_uuid} | wc -l)
       if [ "${is_exist}" -eq 0 ]; then
         if [ ! -d /data-volume ]; then
           sudo mkdir /data-volume
         fi
         sudo echo "UUID=${sdb1_uuid} /data-volume            xfs     defaults        0 0" >> /etc/fstab
         sudo mount -a
       fi
    fi
fi