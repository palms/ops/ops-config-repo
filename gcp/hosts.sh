#!/bin/bash

add_host(){
    host=$1
    ip=`echo "$host" | awk '{print $1}'`
    sed -i "/${ip}/d" /etc/hosts
    echo "$host" >> /etc/hosts
}

add_host "47.100.23.130 config-server"